<?php


namespace App;


use DateTime;
use Psr\Cache\InvalidArgumentException;
use Symfony\Component\Cache\Adapter\FilesystemAdapter;
use Symfony\Contracts\Cache\ItemInterface;

class Hello
{
    /**
     * @param FilesystemAdapter $cache
     * @return mixed
     * @throws InvalidArgumentException
     * Exemple fonction dire bonjour
     */
    public function sayHello(FilesystemAdapter $cache)
    {

       return $cache->get('sayHello', function (ItemInterface $item){

           //Le code suivant crée une date spécifique à l’aide du DateTime constructeur qui spécifie l’année, le mois, le jour.
            $item->expiresAt(new DateTime('2021-09-3'));

            sleep(4);

            return "Bonjour les cocos!!";
        });


    }

    /**
     * @param FilesystemAdapter $cache
     * @return bool
     * @throws InvalidArgumentException
     * Exemple fonction de suppression dire bonjour
     */
    public function deleteSayHello(FilesystemAdapter $cache): bool
    {

        return $cache->delete("sayHello");
    }
}