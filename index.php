<?php

use App\Hello;
use Symfony\Component\Cache\Adapter\FilesystemAdapter;
require 'vendor/autoload.php';


//$cache = new \App\Cache();
/**
 * @see https://symfony.com/doc/current/components/cache/adapters/filesystem_adapter.html#component-cache-filesystem-adapter
 *
 */
$cache = new FilesystemAdapter('cache', 0, dirname(__DIR__ . '/app/cache', 1));


    $hello = new Hello();
    //test par appel simple de la fonction sayHello(FilesystemAdapter $cache)

    //echo $hello->sayHello($cache);
    //$hello->deleteSayHello($cache);

//___________________
/**
 * @see AbstractAdapterTrait;
 */

    //test pour créer un nouvel élément en essayant de l'obtenir à partir du cache
    $sayHello =  $cache->getItem("hello.sayHello");

    //je mets à jour la valeur
    $sayHello->set("Bonjour simplement");

    //je sauvegarde dans mon cache
    $cache->save($sayHello);

    // je recupère l'élément dans mon cache
    $sayHello = $cache->getItem('hello.sayHello');
    //s'il n'existe pas alors
    if (!$sayHello->isHit()) {
        // ... si l'élément n'existe pas dans le cache alors j'appelle simplement la methode sayHello($cache)
        echo $hello->sayHello($cache);
    }

    //je recupère la valeur sauvegardée de mon élément
    $total = $sayHello->get();
    echo $total;

    //supprimer le cache de l'item
    //$cache->deleteItem(['hello.sayHello']);

    //supprimer tout le cache sinon il faut preciser la clé
    //$cache->clear();

/**
 * Certains pools de cache n'incluent pas de mécanisme automatisé pour élaguer les éléments de cache expirés.
 * Par exemple, le cache FilesystemAdapter ne supprime pas les éléments de cache expirés tant qu'un élément n'est pas explicitement demandé et déterminé comme expiré,
 * par exemple, via un appel à Psr\Cache\CacheItemPoolInterface::getItem.
 * L' implémentation ChainAdapter ne contient pas directement de logique d'élagage elle-même.
 * Au lieu de cela, lors de l'appel de la méthode prune() de l'adaptateur de chaîne,
 * l'appel est délégué à tous ses adaptateurs de cache compatibles
 * (et ceux qui ne s'implémentent pas PruneableInterfacesont seront ignorés en silence) :

 */
/*
$cache = new ChainAdapter([
    new FilesystemAdapter(), // DOES implement PruneableInterface
    //...new PdoAdapter(),        // DOES implement PruneableInterface
    //    new PhpFilesAdapter(),   // DOES implement PruneableInterface
]);


$cache->prune();
*/